# librecloud-ansible

Librecoop's ansible roles for docker-compose-based deployment

TBD

## View backups

```bash
export $(ansible-vault view ./inventory/host_vars/dev.librecoop.es/vault | awk -v single_quote="'" '/restic_password/{gsub(single_quote, "", $2); print "RESTIC_PASSWORD="$2} /b2_account_id/{gsub(single_quote, "", $2); print "AWS_ACCESS_KEY_ID="$2} /b2_account_key/{gsub(single_quote, "", $2); print "AWS_SECRET_ACCESS_KEY="$2}' | xargs )
restic -r s3:s3.us-east-005.backblazeb2.com/backup-librecloud-dev/dev-$USER snapshots
```
