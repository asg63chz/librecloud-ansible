Docker-compose Role
===================

This roles deploys a Librecoops cloud that consists of:

- traefik
- backup-bot-two
- odoo
- nextcloud
- mailu
- wordpress
- vaultwarden

Requirements
------------

TBD

For onlyoffice in nextcloud, after deploying:
- install the ONLYOFFICE app
- in the onlyoffice settings:
  - set `ONLYOFFICE Docs address` to `https://onlyoffice.{{ nextcloud_host }}/`
  - set `Secret key` to `{{ onlyoffice_secret_key }}`

Role Variables
--------------

## General

### reset

If set to `True`, the role will remove all running containers and images before deploying. Default: `False`.

### deploy

If set to `True`, the role will deploy the cloud. Default: `True`.

### enable

A list to select which services to deploy. Possible vaules: 'odoo', 'nextcloud', 'mailu', 'wordpress', 'vaultwarden'. Default: `[]`

### make_backup

If set to `True`, the role will make a backup with backup-bot-two. Default: `False`.

### development

If set to `True`, the role will make some shortcuts that might only be acceptable in development environments.
Default is `False`.

### docker_compose_version

Specify a different version of docker compose to install and use.

### acme_email

The email to be used with letsencrypt.

## Backup configuration

### backup_restic_host

URL of the S3 bucket.

### backup_restic_name

Name of the restic repository. Default is `{{ inventory_hostname }}`.

### backup_restic_password

The password to be used to encrypt the restic repository.

### backup_aws_access_key_id

The ID of the S3 account. `AWS_ACCESS_KEY_ID`.

### backup_aws_secret_access_key

The key of the S3 account. `AWS_SECRET_ACCESS_KEY`.

## Odoo configuration

### odoo_image

The Odoo image to deploy. Obviously there might be some assumptions that are made about the behavior of Odoo's
image. This means that that not all images will work, but there's still a chance you want to modify this variable
to select a specific version.

### odoo_host

The hostname used to access Odoo. By default `odoo.{{ inventory_hostname }}` will be used.

### odoo_db_user

The user that Odoo shall use to connect to the database.

### odoo_db_name

The database name that Odoo shall use.

### odoo_db_password

The password for the database.

### odoo_master_password

The master password for the database.

## Nextcloud configuration

### nextcloud_host

The hostname used to access Nextcloud. By default `cloud.{{ inventory_hostname }}` will be used.

### nextcloud_db_user

The user that nextcloud shall use to connect to the database.

### nextcloud_db_name

The database name that nextcloud shall use.

### nextcloud_db_password

The password for the database.

### nextcloud_admin_password

The password for the admin account.

### nextcloud_secret_key

The secret key for nextcloud.

### nextcloud_enable_onlyoffice

Include a onlyoffice container too. Default is `False`

### onlyoffice_secret_key

The secret key for onlyoffice.

## Mailu configuration

### mailu_domain

Main mail domain.

### mailu_host

The hostname used to access mailu. By default `mail.{{ inventory_hostname }}` will be used.

### mailu_enable

A list to select which extra services to deploy. Possible vaules: 'webdav', 'oletools', 'fetchmail', 'clamav'. Default: `[]`

### mailu_secret_key

Set to a randomly generated 16 bytes string.

### mailu_admin_password

The password for the admin@{{ mailu_domain }} account.

## Wordpress configuration

### wordpress_host

The hostname used to access wordpress. By default `{{ inventory_hostname }}` will be used.

### wordpress_db_user

The user that wordpress shall use to connect to the database.

### wordpress_db_name

The database name that wordpress shall use.

### wordpress_db_password

The password for the database.

## Vaultwarden configuration

### vaultwarden_host

The hostname used to access vaultwarden. By default `vault.{{ inventory_hostname }}` will be used.

### vaultwarden_admin_token

The admin token for vaultwarden.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: librecoop.docker-compose
      vars:
        odoo_host: "odoo.librecoop.es"
```

License
-------

GPLv3

Author Information
------------------

librecoop https://www.librecoop.es
